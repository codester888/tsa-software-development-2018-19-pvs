function setup() {
    // module aliases
    Engine = Matter.Engine;
    Render = Matter.Render;
    World = Matter.World;
    Bodies = Matter.Bodies;

    // create an engine
    engine = Engine.create();

    console.log(document.body);

    // create a renderer
    render = Render.create({
        element: document.body,
        engine: engine
    });

    // create two boxes and a ground
    boxA = Bodies.rectangle(400, 200, 80, 80);
    boxB = Bodies.rectangle(450, 50, 80, 80);
    ground = Bodies.rectangle(400, 610, 810, 60, { isStatic: true });

    // add all of the bodies to the world
    World.add(engine.world, [boxA, boxB, ground]);

    // run the engine
    Engine.run(engine);

    // run the renderer
    Render.run(render);

    console.log("Finished setting up physics simulation")
}