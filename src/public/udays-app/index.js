function openNav() {
	document.getElementById("mySidenav").style.width = "300px";
  }
function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
  }
window.addEventListener('load', function() {
	var allBodies = [];
	var unStatic = [];
	var HEIGHT = window.innerHeight-100;
	var WIDTH = window.innerWidth-20;
	var canvas = document.getElementById('world');
	var engine = Matter.Engine.create();
	var world = engine.world;
	var Bodies = Matter.Bodies;
	var draggingEnabled = true;
	var render = Matter.Render.create({
		canvas: canvas,
		engine: engine,
		options: {
			width: WIDTH,
			height: HEIGHT,
			wireframes: false,
			showAngleIndicator: false
		}
	});
	var xValue, yValue, Density, Friction, AirFriction, size, Radius, Degrees, Sides;
	Friction = 0.1;
	AirFriction = 0.1;
	Radius = 10;
	Density = 0.001;
	Degrees = 180;
document.getElementById("platformSub").addEventListener("click", function() {
	xValue = document.getElementById("scroller1").value;
	yValue = document.getElementById("scroller2").value;
	size = document.getElementById("scroller6").value;
	Sides = document.getElementById("scroller10").value;

})
document.getElementById("Run").addEventListener("click", function(){
	draggingEnabled = true;
});
document.getElementById("Stop").addEventListener("click", function() {
	draggingEnabled = false;
})
document.getElementById("un-staticChangeProperty").addEventListener("click", function() {
	Radius = document.getElementById("scroller3").value;
	Density = document.getElementById("scroller4").value;
	AirFriction = document.getElementById("scroller5").value;
	Friction = document.getElementById("scroller7").value;

});
	//Make interactive
	var mouseConstraint = Matter.MouseConstraint.create(engine, { //Create Constraint
		element: canvas,
		constraint: {
			render: {
	        	visible: false
	    	},
	    	stiffness:0.8
	    }
	});
	Matter.World.add(world, mouseConstraint);
	function MatterAdder() {
		var temp = 	Matter.Bodies.polygon((xValue*WIDTH)/100, (yValue*HEIGHT)/100, Sides/10+3, 6*size, {isStatic: true, 
			render: {
				fillStyle: 'yellow',
				strokeStyle: 'yellow',
				lineWidth: 3
	 }})
		Matter.World.add(world, temp);
		allBodies.push(temp);
	}
	document.getElementById('platformSub').addEventListener("click", function() {
		MatterAdder();
		console.log(allBodies);
	});
	document.getElementById('prevDelete').addEventListener("click", function() {
		Matter.Composite.remove(world, allBodies[allBodies.length-1]);
		allBodies.pop();
	});
	addEventListener("click", function(event) {
		var temporary = Matter.Bodies.circle(event.clientX, event.clientY, Radius, {
			density: Density/10000,
			frictionAir: AirFriction/1000,
			friction: Friction/1000,
		});
		if (draggingEnabled) {
			Matter.World.add(world, temporary);
			unStatic.push(temporary);
		}
	});
	document.getElementById("un-staticDeleter").addEventListener("click", function() {
		Matter.Composite.remove(world, unStatic[unStatic.length-1]);
		unStatic.pop()
	})
	//Start the engine
	Matter.Engine.run(engine);
	Matter.Render.run(render);
}); 