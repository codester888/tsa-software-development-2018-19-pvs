//# Imports
const express = require("express");
const sass = require("sass")
const fs = require("fs")
const path = require("path")
const logger = require("morgan")

//# Sass -> css rendering
function renderSassSync(sourceDirectory, buildDirectory) {
    fs.readdirSync(sourceDirectory).forEach(file => {
      fs.writeFileSync(
        path.join(buildDirectory, path.parse(file).name + ".css"),
  
        sass.renderSync({file: path.join(sourceDirectory, file)}).css,
  
        (err) => {if (err) throw err;}
      );
    });
  }
renderSassSync("./public/source/stylesheets/", "./public/stylesheets/");  

//# App setup
var app = express();
app.locals.title = "PhyRoom";
app.locals.email = "gatlenculp@gmail.com";

// Query strings for optional, parameter strings for required

//# Public directory
//> Setting up a directory publically accessible by the user without having to go through a full server request
    //> If you have an app.get with the same directory as something from the public directory, the app.get takes over
app.use(express.static(path.join(__dirname, 'public')));

//# Morgan logger
app.use(logger("dev"))

//# View Engine
  //> Uses app.render(file name from views)
      //> Can pass a set of objects into the page itself for rendering
app.set('views', path.join(__dirname, 'views'));
app.set("view engine", "pug");

app.get('/', (req, res) => {
    res.render("index.pug",
        {pageTitle: "PhyRoom Home"}
    );
});

app.get('/info', function(req, res){
    res.send("App title: " + app.locals.title + "<br>" +
            "App email: " + app.locals.email + "<br>");
  });

app.get('/sandbox', (req, res) => {
    res.render("sandbox.pug",
        {pageTitle: "Sandbox"}
    );
});

app.get('/api/sandbox/:sceneName/:objects', (req, res) => {
    res.send(`sceneName: ${req.params.sceneName} <br>
            objects: ${req.params.objects}`);
});

//# Launch app
//> Use $env:PORT=(VALUE) to set port
const port = process.env.PORT || 80;
app.listen(port, () => console.log(`Listening on port ${port}...`));